import { useRef, useState } from 'react'

import axios from 'axios'

function App() {

  const [fileCount, setFileCount] = useState<number>(0)
  const [fileList, setFileList] = useState<File[]>([])
  const files = useRef<HTMLInputElement>(null)
  const [fileNane, setFileNane] = useState<string>("")

  const sendFilesOnCompress = async () => {

    const formDataFile = new FormData();

    fileList.forEach(file => formDataFile.append("file", file));

    await axios.post("/compressor", formDataFile, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
      .then(res => setFileNane(res.data.zipfile))

  }


  const ListFileItem = ({ file }: { file: File }) => {
    return (
      <div className='w-full h-10 bg-zinc-200 rounded-lg flex items-center justify-between px-4'>
        <p>{file.name}</p>
        <p>{file.size} байт</p>
      </div>
    )
  }

  const countFiles = () => {

    setFileList([])

    if (files.current?.files) {
      let filess = Array.from(files.current.files);

      // Фильтруем файлы по типу
      filess = filess.filter(file => file.type === "text/css");

      // Создаем новый DataTransfer объект
      const dataTransfer = new DataTransfer();

      // Добавляем отфильтрованные файлы в DataTransfer
      filess.forEach(file => {
        dataTransfer.items.add(file);
      });

      // Обновляем files.current.files с новым FileList
      files.current.files = dataTransfer.files;

      // Устанавливаем количество файлов
      setFileCount(dataTransfer.files.length);

      setFileList(filess);
    }
  }

  const clearFileInput = () => {
    if (files.current) {
      files.current.value = '';
    }
  };

  const download = async () => {
    try {
      const url = `/compressor/${fileNane}`;
      const a = document.createElement('a');
      a.href = url;
      // @ts-ignore
      a.download = fileNane; // Здесь у нас строка fileName
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      setFileCount(0)
      setFileList([])
      setFileNane("")
      clearFileInput()

    } catch (error) {
      console.error('Error during file download:', error);
    }
  };

  return (
    <div className='w-full h-[100vh] bg-zinc-500 flex items-center justify-center'>
      <div className='w-[900px] max-h-[400px] bg-white rounded-lg p-4 shadow-lg'>
        <h1 className='text-3xl font-bold'>CSSCompressor</h1>
        <p><small>Маленький сервис по сжатию CSS β-test</small></p>
        <div className='mt-10 w-full flex gap-3'>
          <div className='w-full'>
            <div className='relative'>
              <input ref={files} onChange={countFiles} className='opacity-0 invisible absolute' type="file" name="files" id="files" multiple />
              <label htmlFor="files">
                <div className='flex cursor-pointer'>Выбрать файл</div>
                <div className='flex space-x-4'>
                  <small>Выберите файл для сжатия</small>
                  <small>Файлов: {fileCount}</small>
                </div>
              </label>
            </div>
            <div className='mt-4 space-x-4 flex items-center'>
              <button className='bg-green-600 px-[20px] py-[5px] rounded-lg text-white' type='button' onClick={sendFilesOnCompress}>Сжать</button>
              {fileNane != "" || fileNane.length > 0 ? <a onClick={download} className='flex w-[max-content] bg-red-600 px-[20px] py-[5px] rounded-lg text-white cursor-pointer'>Скачать</a> : null}
            </div>

            <div className='mt-4 w-full p-2 bg-zinc-200 rounded-lg flex flex-col space-y-2'>
              <small className='1 leading-6'>Система автоматически откидывает файлы которые не являются CSS.<br />В случае если вы добавили все файлы которые были в директории, то не беспойкойтись, они к нам не попадут.</small>
            </div>
          </div>
          <div className='w-full max-h-[265px] overflow-auto'>
            <div className='flex flex-col space-y-4'>
              {fileList.length > 0 ? fileList.map((file, index) => {
                return (
                  <ListFileItem file={file} key={index} />
                )
              }) : <div className='w-full'>Пока тут нет никаких файлов</div>}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
